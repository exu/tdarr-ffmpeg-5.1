## Building

1. Download the "root" folder from the [Tdarr Repo (direct link)](https://github.com/HaveAGitGat/Tdarr/tree/master/docker/root)
2. Place this folder in here, next to the dockerfile
3. Build the server and the node container

### Server

`$ buildah bud -t tdarr-server-ffmpeg5.1:2 -t tdarr-server-ffmpeg5.1:latest -f debian-bullseye.dockerfile`

### Node

`$ buildah bud -t tdarr-node-ffmpeg5.1:2 -t tdarr-node-ffmpeg5.1:latest -f debian-bullseye.dockerfile --build-arg "MODULE=Tdarr_Node"`

## Versions

Available versions can be found in [this json file](https://f000.backblazeb2.com/file/tdarrs/versions.json)
